# thanos-rs

A program which can delete half the files in any directory, instantly decreasing the size of your project by 50%!

## W A R N I N G

### THANOS-RS DOES NOT DISCRIMINATE!

### ANY FILE CAN BE DELETED

### EVEN HIDDEN FILES, `.git`, AND WHATEVER ELSE.

## :)
