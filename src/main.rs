#![feature(drain_filter)]
#![warn(clippy::pedantic)]

use std::{env::current_dir, fs, path::Path, process};

use argh::FromArgs;
use colored::Colorize;
use indicatif::ProgressIterator;
use rand::{distributions::Bernoulli, prelude::Distribution, thread_rng};

fn main() {
    let o: Args = argh::from_env();

    if let Subcommands::SnapFingers(opts) = o.nested {
        snap(&opts);
    }
}

fn snap(opts: &SnapFingers) {
    if !confirm_implications(opts.with_infinity_gauntlet) {
        process::exit(1);
    }

    let dir = current_dir().unwrap();
    let mut files = walk_folder(&dir, 0);

    let mut rng = thread_rng();
    let b = Bernoulli::new(0.500).unwrap();

    let to_delete = files
        .drain_filter(|_| b.sample(&mut rng))
        .collect::<Vec<_>>();

    for x in to_delete.iter().progress() {
        fs::remove_file(x).unwrap_or_default();
    }
}

fn walk_folder(path: &Path, iter: u8) -> Vec<String> {
    if iter >= 100 {
        return vec![];
    }
    let mut files = Vec::with_capacity(100);
    for x in fs::read_dir(path).unwrap() {
        match x {
            Ok(entry) => {
                let ft = entry.file_type().unwrap();
                if ft.is_file() {
                    files.push(entry.path().to_str().unwrap().to_string());
                } else if ft.is_dir() {
                    files.append(&mut walk_folder(&entry.path(), iter + 1));
                }
            }
            _ => continue,
        }
    }

    files
}

fn confirm_implications(full_power: bool) -> bool {
    let dir = current_dir().unwrap();
    let cwd = dir.to_string_lossy();

    if full_power {
        println!(
            "{}{}{}",
            "Do you understand that when Thanos snaps his fingers, half of the files inside `"
                .red(),
            cwd.yellow(),
            "` will be destroyed?".red()
        );
    } else {
        println!(
            "{}{}",
            "Where is my infinity gauntlet?! ".red(),
            "(pass `--with-infinity-gauntlet` to run)".bright_black()
        );
        process::exit(1);
    }

    dialoguer::Confirm::new().interact().unwrap()
}

#[derive(FromArgs, PartialEq, Debug)]
/// various options for thanos-rs
struct Args {
    #[argh(subcommand)]
    nested: Subcommands,
}

#[derive(FromArgs, PartialEq, Debug)]
#[argh(subcommand)]
enum Subcommands {
    SnapFingers(SnapFingers),
    Null(Null),
}

#[derive(FromArgs, PartialEq, Debug)]
#[argh(subcommand, name = "snap-fingers")]
/// snaps fingers
struct SnapFingers {
    /// whether to use the full power version
    #[argh(switch)]
    with_infinity_gauntlet: bool,
}

#[derive(FromArgs, PartialEq, Debug)]
#[argh(subcommand, name = "null")]
/// does nothing
struct Null {}
